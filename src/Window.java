import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by amen on 9/8/17.
 */
public class Window extends JFrame {

    private boolean eqClicked = false;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Window().setVisible(true);
            }
        });
    }

    public Window() {
        setContentPane(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setPreferredSize(new Dimension(500, 400));

        ///
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof JButton) {
                    JButton button = (JButton) source;
                    // czytam zawartość buttona (jaką ma na sobie etykiete)
                    String content = button.getText();

                    if (eqClicked) {
                        equation.setText("");
                        eqClicked = false;
                    }

                    // dopisuje do pola tekstowego
                    // jego zawartość + content buttona
                    String[] splits = equation.getText().split(" ");
                    String ostatnia_liczba = splits[splits.length - 1];
                    if (ostatnia_liczba.contains(".") && content.equals(".")) {
                        return;
                    }
                    equation.setText((equation.getText() + content).replace("..", "."));
                }
            }
        };

        ActionListener listener_operators = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof JButton) {
                    JButton button = (JButton) source;
                    // czytam zawartość buttona (jaką ma na sobie etykiete)
                    String content = button.getText();

                    if (eqClicked) {
                        eqClicked = false;
                    }

                    // dopisuje do pola tekstowego
                    // jego zawartość + content buttona
                    equation.setText(equation.getText().trim());
                    equation.setText(equation.getText() + " " + content + " ");
                }
            }
        };

        // operandy : 0 1 2 3 4 5 6 7 8 9 .
        a0Button.addActionListener(listener);
        a1Button.addActionListener(listener);
        a2Button.addActionListener(listener);
        a3Button.addActionListener(listener);
        a4Button.addActionListener(listener);
        a5Button.addActionListener(listener);
        a6Button.addActionListener(listener);
        a7Button.addActionListener(listener);
        a8Button.addActionListener(listener);
        a9Button.addActionListener(listener);
        buttonDecimal.addActionListener(listener);

        // operatory : + / - * ( )
        buttonAdd.addActionListener(listener_operators);
        buttonSub.addActionListener(listener_operators);
        buttonDiv.addActionListener(listener_operators);
        buttonMul.addActionListener(listener_operators);
        buttonLB.addActionListener(listener_operators);
        buttonRB.addActionListener(listener_operators);

        // Clear
        cButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                equation.setText("");
            }
        });

        // remove one sign
        buttonR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!equation.getText().isEmpty()) {
                    equation.setText(equation.getText().trim());
                    equation.setText(equation.getText().trim().substring(0, equation.getText().length() - 1).trim());
                }
            }
        });

        buttonEq.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ONP onp = new ONP(equation.getText().trim());

                // obliczenia
                Double wynik = onp.countONP(onp.convertToONP());

                // ustawiam pole tekstowe
                equation.setText(wynik.toString());
                eqClicked = true;
            }
        });

        ///
        pack();
    }

    private JPanel mainPanel;
    private JPanel northPanel;
    private JPanel southPanel;
    private JTextField equation;
    private JPanel leftPanel;
    private JPanel rightPanel;
    private JPanel leftInternalPanel;
    private JPanel rightInternalPanel;
    private JButton a7Button;
    private JButton a4Button;
    private JButton a3Button;
    private JButton a8Button;
    private JButton a5Button;
    private JButton a9Button;
    private JButton a6Button;
    private JButton a2Button;
    private JButton a1Button;
    private JButton a0Button;
    private JButton buttonDecimal;
    private JButton buttonMul;
    private JButton buttonDiv;
    private JButton buttonSub;
    private JButton buttonAdd;
    private JButton buttonEq;
    private JButton buttonLB;
    private JButton buttonRB;
    private JButton cButton;
    private JButton buttonR;

}
